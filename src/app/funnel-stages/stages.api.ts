import { Application } from "express";
import routes from "./stages.route";
import db from "./stages.db";
import { PathEnum } from "../../models/paths";

const api=(app:Application):void=>{
    app.set('dbStages',db);
    app.use(`/${PathEnum.PREFIX}/${PathEnum.STAGES}`,routes);
}

export default api;
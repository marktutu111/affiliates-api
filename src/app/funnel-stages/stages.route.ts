import *as express from "express";
const router=express.Router();
import Handler from "./stages.handler";

router.get('/get',Handler.stages);
router.post('/add',Handler.addstage);
router.delete('/delete/:id',Handler.removeStage);

export default router;
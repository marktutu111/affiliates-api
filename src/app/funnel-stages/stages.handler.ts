import { Request,Response } from "express";
import { Document, Model } from "mongoose";

const stages=async(request:Request,response:Response)=>{
    try {
        const dbStages:Model<any>=request.app.get('dbStages');
        const results=await dbStages.find({}).sort({createdAt:-1});
        return response.json(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:true,
                message:err.message,
            }
        )
    }
}

const removeStage=async(request:Request,response:Response)=>{
    try {
        const {id}=request.params;
        const dbStages:Model<any>=request.app.get('dbStages');
        const results:Document=await dbStages.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!results || !results._id){
            throw Error(
                'Stage could not be deleted'
            )
        }
        return response.json(
            {
                success:true,
                message:'Funnel stage added successfully',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:true,
                message:err.message,
            }
        )
    }
}


const addstage=async(request:Request,response:Response)=>{
    try {
        const data=request.body;
        const dbStages:Model<any>=request.app.get('dbStages');
        const results=await dbStages.create(data);
        return response.json(
            {
                success:true,
                message:'Funnel stage added successfully',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:true,
                message:err.message,
            }
        )
    }
}



export default {
    stages,
    addstage,
    removeStage
}
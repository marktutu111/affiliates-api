import { Application } from "express";
import { PathEnum } from "../../models/paths";
import db from "./users.db";
import route from "./users.route";

const api=(app:Application):void=>{
    app.set('dbUsers',db);
    app.use(`/${PathEnum.PREFIX}/${PathEnum.USERS}`,route);
}


export default api;
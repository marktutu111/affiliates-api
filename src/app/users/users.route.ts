import * as express from "express";
const router=express.Router();
import Handler from "./users.handler";

router.get('/get',Handler.fetchUsers);
router.post('/new',Handler.newuser);
router.put('/update',Handler.modify);
router.post('/login',Handler.login);
router.put('/password/update',Handler.updatePassword);


export default router;
import *as mongoose from "mongoose";
import *as bcrypt from "bcrypt";

const schema=new mongoose.Schema(
    {
        name:String,
        email:{
            type:String,
            unique:true
        },
        phone:{
            type:String,
            unique:true
        },
        password:String,
        active:{
            type:Boolean,
            default:false
        },
        blocked:{
            type:Boolean,
            default:false
        },
        role:String,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

schema.pre('save', async function (next) {
    this['updatedAt']=Date.now();
    // only hash the password if it has been modified (or is new)
    if (!this.isModified('password')) return next();
    // generate a salt
    const SALT=await bcrypt.genSalt(10);
    // generate hashed password;
    const _hash=await bcrypt.hash(this['password'], SALT);
    // Replace password with hashed password;
    this['password']=_hash;
    return next();
});

export default mongoose.model('users',schema);
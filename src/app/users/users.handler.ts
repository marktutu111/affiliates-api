import { Request, Response } from "express";
import { Document, Model } from "mongoose";
import *as bcrypt from "bcrypt";
import { AuthTypes } from "../../models/paths";





const login=async(request:Request,response:Response)=>{
    try {
        const {email,password}=request.body;
        const dbUsers:Model<any>=request.app.get('dbUsers');
        const result:Document=await dbUsers.findOne(
            {
                email:email
            }
        );
        if(!result || !result._id){
            throw Error(
                'Email does not match any record'
            )
        }
        const _match=await bcrypt.compare(password,result['password']);
        if(!_match){
            throw Error(
                'Password is not valid'
            )
        };
        return response.json(
            {
                success:true,
                message:'Authentication successfull',
                data:{
                    type:AuthTypes.ADMIN,
                    user:result,
                    token:''
                }
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const fetchUsers=async(request:Request,response:Response)=>{
    try {
        const dbUsers:Model<any>=request.app.get('dbUsers');
        const results=await dbUsers.find({}).sort({createdAt:-1});
        return response.json(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const newuser=async(request:Request,response:Response)=>{
    try {
        const data=request.body;
        const dbUsers:Model<any>=request.app.get('dbUsers');
        const found=await dbUsers.findOne(
            {
                $or:[
                    {
                        email:data.email
                    },
                    {
                        phone:data.phone
                    }
                ]
            }
        );
        if(found && found._id){
            throw Error(
                'User email or phone number is already registered in the system'
            )
        }
        const results=await dbUsers.create(data);
        return response.json(
            {
                success:true,
                message:'User account created successfully',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const updatePassword=async(request:Request,response:Response)=>{
    try {
        const {oldpassword,newpassword,id}=request.body;
        const dbUsers:Model<any>=request.app.get('dbUsers');
        const account:Document=await dbUsers.findOne(
            {
                _id:id
            }
        );
        if(!account || !account._id){
            throw Error(
                'Account does match any record'
            )
        }
        const match=await bcrypt.compare(oldpassword,account['password']);
        if(!match)throw Error('Old password does not match any record');
        if(oldpassword===newpassword)throw Error('Old password cannot be the same as new password');
        const SALT = await bcrypt.genSalt(10);
        const _hash=await bcrypt.hash(newpassword, SALT);
        await account.update(
            {
                password:_hash,
                active:true
            }
        )
        return response.json(
            {
                success:true,
                message:'Password updated successfully,kindly login again to continue',
                data:account
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const modify=async(request:Request,response:Response)=>{
    try {
        const {id,data}=request.body;
        const dbUsers:Model<any>=request.app.get('dbUsers');
        const result:Document=await dbUsers.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{new:true}
        )
        if(!result || !result._id){
            throw Error(
                'User account update failed'
            )
        };
        return response.json(
            {
                success:true,
                message:'Account updated successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    login,
    fetchUsers,
    newuser,
    modify,
    updatePassword
}
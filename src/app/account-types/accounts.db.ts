import *as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        name:{
            type:String,
            uppercase:true
        },
        description:String,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model('accounts',schema);
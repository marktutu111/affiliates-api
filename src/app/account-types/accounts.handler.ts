import { Request,Response } from "express";
import { Model } from "mongoose";

const accounts=async(request:Request,response:Response)=>{
    try {
        const dbStages:Model<any>=request.app.get('dbAccounts');
        const results=await dbStages.find({}).sort({createdAt:-1});
        return response.json(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:true,
                message:err.message,
            }
        )
    }
}

const remove=async(request:Request,response:Response)=>{
    try {
        const {id}=request.params;
        const dbAccounts:Model<any>=request.app.get('dbAccounts');
        const results=await dbAccounts.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!results || !results._id){
            throw Error(
                'Account deleted failed'
            )
        }
        return response.json(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:true,
                message:err.message,
            }
        )
    }
}


const addAccount=async(request:Request,response:Response)=>{
    try {
        const data=request.body;
        const dbStages:Model<any>=request.app.get('dbAccounts');
        const results=await dbStages.create(data);
        return response.json(
            {
                success:true,
                message:'Funnel stage added successfully',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:true,
                message:err.message,
            }
        )
    }
}



export default {
    accounts,
    addAccount,
    remove
}
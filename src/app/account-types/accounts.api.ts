import { Application } from "express";
import routes from "./accounts.route";
import db from "./accounts.db";
import { PathEnum } from "../../models/paths";

const api=(app:Application):void=>{
    app.set('dbAccounts',db);
    app.use(`/${PathEnum.PREFIX}/${PathEnum.ACCOUNTS}`,routes);
}

export default api;
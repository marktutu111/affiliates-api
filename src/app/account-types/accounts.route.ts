import *as express from "express";
const router=express.Router();
import Handler from "./accounts.handler";

router.get('/get',Handler.accounts);
router.post('/add',Handler.addAccount);
router.delete('/delete/:id',Handler.remove);

export default router;
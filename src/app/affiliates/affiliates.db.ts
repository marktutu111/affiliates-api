import *as mongoose from "mongoose";
import *as bcrypt from "bcrypt";

const schema=new mongoose.Schema(
    {
        fname:String,
        lname:String,
        phone:String,
        email:String,
        profession:String,
        organization:String,
        city:String,
        region:String,
        blocked:Boolean,
        password:String,
        gender:String,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
);

schema.pre('save', async function (next) {
    this['updatedAt']=Date.now();
    // only hash the password if it has been modified (or is new)
    if (!this.isModified('password')) return next();
    // generate a salt
    const SALT=await bcrypt.genSalt(10);
    // generate hashed password;
    const _hash=await bcrypt.hash(this['password'], SALT);
    // Replace password with hashed password;
    this['password']=_hash;
    return next();
});


export default mongoose.model('affiliates',schema);
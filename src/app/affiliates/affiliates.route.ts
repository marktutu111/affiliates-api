import * as express from "express";
const router=express.Router();
import Handler from "./affiliates.handler";

router.get(`/get`,Handler.fetchAffiliates);
router.post(`/new`,Handler.add);
router.post('/login',Handler.login);
router.delete(`/delete/:id`,Handler.deleteAff);
router.put(`/update`,Handler.modify);
router.put('/password/update',Handler.updatePassword);


export default router;
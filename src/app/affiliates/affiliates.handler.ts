import { Request, Response } from "express";
import { Document, Model } from "mongoose";
import *as bcrypt from "bcrypt";
import { AuthTypes } from "../../models/paths";


const updatePassword=async(request:Request,response:Response)=>{
    try {
        const {oldpassword,newpassword,id}=request.body;
        const dbAffiliates:Model<any>=request.app.get('dbAffiliates');
        const account:Document=await dbAffiliates.findOne(
            {
                _id:id
            }
        );
        if(!account || !account._id){
            throw Error(
                'Account does match any record'
            )
        }
        const match=await bcrypt.compare(oldpassword,account['password']);
        if(!match)throw Error('Old password does not match any record');
        const SALT = await bcrypt.genSalt(10);
        const _hash=await bcrypt.hash(newpassword, SALT);
        await account.update(
            {
                password:_hash
            }
        )
        return response.json(
            {
                success:true,
                message:'Password updated successfully',
                data:account
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const login=async(request:Request,response:Response)=>{
    try {
        const {email,password}=request.body;
        const dbAffiliates:Model<any>=request.app.get('dbAffiliates');
        const result:Document=await dbAffiliates.findOne(
            {
                email:email
            }
        );
        if(!result || !result._id){
            throw Error(
                'Email does not match any record'
            )
        }
        const _match=await bcrypt.compare(password,result['password']);
        if(!_match){
            throw Error(
                'Password is not valid'
            )
        };
        return response.json(
            {
                success:true,
                message:'Authentication successfull',
                data:{
                    type:AuthTypes.AFFILIATE,
                    user:result,
                    token:''
                }
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const fetchAffiliates=async(request:Request,response:Response)=>{
    try {
        const dbAffiliates:Model<any>=request.app.get('dbAffiliates');
        const results=await dbAffiliates.find({}).sort({createdAt:-1});
        return response.json(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const deleteAff=async(request:Request,response:Response)=>{
    try {
        const {id,data}=request.body;
        const dbAffiliates:Model<any>=request.app.get('dbAffiliates');
        const result:Document=await dbAffiliates.findOneAndRemove(
            {
                _id:id
            }
        )
        if(!result || !result._id){
            throw Error(
                'Affiliate account delete failed'
            )
        };
        return response.json(
            {
                success:true,
                message:'Account deleted successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const modify=async(request:Request,response:Response)=>{
    try {
        const {id,data}=request.body;
        const dbAffiliates:Model<any>=request.app.get('dbAffiliates');
        const result:Document=await dbAffiliates.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{new:true}
        )
        if(!result || !result._id){
            throw Error(
                'Affiliate account update failed'
            )
        };
        return response.json(
            {
                success:true,
                message:'Account updated successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const add=async(request:Request,response:Response)=>{
    try {
        const payload=request.body;
        const dbAffiliates:Model<any>=request.app.get('dbAffiliates');
        const result:Document=await dbAffiliates.create(payload);
        if(!result || !result._id){
            throw Error(
                'Account not be created'
            )
        };
        return response.json(
            {
                success:true,
                message:'Account registered successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    fetchAffiliates,
    add,
    modify,
    deleteAff,
    login,
    updatePassword
}
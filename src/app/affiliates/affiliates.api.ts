import { Application } from "express";
import { PathEnum } from "../../models/paths";
import db from "./affiliates.db";
import routes from "./affiliates.route";

const api=(app:Application)=>{
    app.set('dbAffiliates',db);
    app.use(`/${PathEnum.PREFIX}/${PathEnum.AFFILIATES}`,routes);
}

export default api;
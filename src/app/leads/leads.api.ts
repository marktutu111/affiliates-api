import { Application } from "express";
import routes from "./leads.route";
import db from "./leads.db";
import { PathEnum } from "../../models/paths";

const api=(app:Application)=>{
    app.set('dbLeads',db);
    app.use(`/${PathEnum.PREFIX}/${PathEnum.LEADS}`,routes);
}


export default api;
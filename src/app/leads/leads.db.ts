import *as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        createdBy:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'affiliates'
        },
        name:String,
        email:String,
        description:String,
        location:String,
        businessType:String,
        cnumber:String,
        cperson:String,
        stage:{
            type:[String],
            default:[]
        },
        notes:{
            type:[String],
            default:[]
        },
        assigned:{
            type:[{type:mongoose.Schema.Types.ObjectId,ref:'users'}],
            default:[]
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model('leads',schema);
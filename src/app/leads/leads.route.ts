import *as express from "express";
const router=express.Router();
import Handler from "./leads.handler";

router.get('/get',Handler.getLeads);
router.get('/get/:id',Handler.getLeadsById);
router.post('/create',Handler.createLeads);
router.put('/update',Handler.updateLead)

export default  router;
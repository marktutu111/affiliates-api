import { Request, Response } from "express";
import { Document, Model } from "mongoose";


const updateLead=async(request:Request,response:Response)=>{
    try {
        const {id,data}=request.body;
        const dbLeads:Model<any>=request.app.get('dbLeads');
        const result=await dbLeads.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{new:true}
        );
        if(!result || !result._id){
            throw Error(
                'Lead update failed'
            )
        }
        return response.json(
            {
                success:true,
                message:'Lead updated successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getLeadsById=async(request:Request,response:Response)=>{
    try {
        const {id}=request.params;
        const dbLeads:Model<any>=request.app.get('dbLeads');
        const result:Array<Document>=await dbLeads.find(
            {
                $or:[
                    {
                        createdBy:id
                    },
                    {
                        _id:id
                    }
                ]
            }
        ).sort({createdAt:-1}).populate('createdBy').populate('assigned')
        return response.json(
            {
                success:true,
                message:'Leads loaded successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const createLeads=async(request:Request,response:Response)=>{
    try {
        const data=request.body;
        const dbLeads:Model<any>=request.app.get('dbLeads');
        const result:Document=await dbLeads.create(data);
        if(!result || !result._id){
            throw Error(
                'Lead could not be created'
            )
        }
        return response.json(
            {
                success:true,
                message:'Lead created successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getLeads=async(request:Request,response:Response)=>{
    try {
        const dbLeads:Model<any>=request.app.get('dbLeads');
        const result:Array<Document>=await dbLeads.find({}).sort({createdAt:-1}).populate('createdBy').populate('assigned');
        return response.json(
            {
                success:true,
                message:'Leads loaded successfully',
                data:result
            }
        )
    } catch (err) {
        return response.json(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    getLeads,
    getLeadsById,
    createLeads,
    updateLead
}

import *as express from "express";
import *as mongoose from "mongoose";
import *as morgon from "morgan";
import { Db_config } from "./config/database.config";
import { _server } from "./config/server.config";
import *as bodyParser from "body-parser";
import *as cors from "cors";

import Affiliates from "./app/affiliates/affiliates.api";
import Leads from "./app/leads/leads.api";
import Stages from "./app/funnel-stages/stages.api";
import Accounts from './app/account-types/accounts.api';
import Users from "./app/users/users.api";

const start=async()=>{
    try {

        const app=express();
        app.use(cors());
        app.use(morgon('combined'));
        app.use(bodyParser.json());

        Affiliates(app);
        Leads(app);
        Stages(app);
        Accounts(app);
        Users(app);

        await mongoose.connect(Db_config.URL, { useNewUrlParser: true });
        app.listen(_server.port);
        console.log('server running on port: '+_server.port);

    } catch (err) {
       console.log(err); 
    }
}


start();

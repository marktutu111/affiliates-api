export enum PathEnum {
    'PREFIX'='afm',
    'AFFILIATES'='affiliates',
    'LEADS'='leads',
    'STAGES'='stages',
    'ACCOUNTS'='accounts',
    'USERS'='users'
}


export enum AuthTypes {
    'AFFILIATE'='AF',
    'ADMIN'='ADMIN'
}